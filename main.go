package main

// go install pluralsight-the-go-programming-language/greeting
import (
  "pluralsight-the-go-programming-language/greeting"
  "fmt"
  "time"
)

func RenameToFrog(r greeting.Renameable) {
  r.Rename("Frog")
}

func main() {
  // var s = greeting.Salutation("Bob", "Hello")
  
  salutations := greeting.Salutations{
    { "Bob", "Hello" },
    { "Joe", "Hi" },
    { "Mary", "What is up?" },    
  }
  
  fmt.Fprintf(&salutations[0], "The count is %d", 10)
  
  salutations[0].Rename("John")
  RenameToFrog(&salutations[0])
  fmt.Println(salutations[0])
  
  done := make(chan bool, 2)
  
  go func() {
    salutations.Greet(greeting.CreatePrintFunction("<C>"), true, 6)
    done <- true
    time.Sleep(100 * time.Millisecond)
    done <- true
    fmt.Println("Done!")
  }()
  
  salutations.Greet(greeting.CreatePrintFunction("?"), true, 6)
  
  if v := <- done; v {
    salutations.Greet(greeting.CreatePrintFunction("?##"), true, 6)
  }
  
  c1 := make(chan greeting.Salutation)
  c2 := make(chan greeting.Salutation)
  
  go salutations.ChannelGreeter(c1)
  go salutations.ChannelGreeter(c2)
  
  for {
    select {
      case s, ok := <- c1:
        if ok {
          fmt.Println(s, ":1")
        } else {
          return
        }
      case s, ok := <- c2:
        if ok {
          fmt.Println(s, ":2")
        } else {
          return
        }
      default:
        fmt.Println("Waiting...")
    }
  }
  
  //for s := range c {
  //  fmt.Println(s.Name)
  //}
    
  //for {
  //  time.Sleep(100 * time.Millisecond)
  //}  
}