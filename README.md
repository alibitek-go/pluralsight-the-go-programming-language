# Pluralsight - The Go Programming Language
### Notes taken while following The Go Programming Language course at Pluralsight
- Go Overview
    * Example: [Go Tetris](https://github.com/nsf/gotris/blob/master/gotris.go)
- Go Development
    * IDEs
        - GoEclipse
        - [IntelliJ IDEA Go Plugin](https://github.com/mtoader/google-go-lang-idea-plugin)
    * Text Editors
        - [http:/go-lang.cat-v.org/text-editors](http:/go-lang.cat-v.org/text-editors)

- Variables, Types and Pointers
    * Basic Types:
        - bool
        - string
        - int, int8, int16, int32, int64
        - uint, uint8, uint16, uint32, uint64, uintptr
        - byte (uint8)
        - rune (int32), like a char
        - float32, float64
        - complex64, complex128
    * Other Types:
        - Array
        - Slice
        - Struct
        - Pointer
        - Function
        - Interface
        - Map
        - Channel
- Functions
    - Example of of function usage: [http://golang.org/doc/codewalk/functions](http://golang.org/doc/codewalk/functions)
    - Function types: [http://golang.org/ref/spec#Function_types](http://golang.org/ref/spec#Function_types)
- Branching
    - if
    - switch
        - No default fall through
        - Break statements are not required
        - Don't need an expression
        - Cases can be expressions
        - Switch on types
- Loops
    - Types of Ranges:
        - Array or slice
        - String
        - Map
        - Channel
    - More on range and for:
        - [http://golang.org/doc/effective_go.html#for](http://golang.org/doc/effective_go.html#for)
        - [http://golang.org/ref/spec#For_statements](http://golang.org/ref/spec#For_statements)
- Maps
    - Keys have to have equality operator defined
    - Maps are reference types
    - Not thread safe!
    - [http://golang.org/doc/effective_go.html#maps](http://golang.org/doc/effective_go.html#maps)
- Slices
- `var s []int = make([]int, 3, 5)`  
    - Properties of a slice:
        - Fixed size (but can be reallocated with append)
        - Type is slice of underyling type (doesn't have length)
        - Use make to initialize otherwise is nil
        - Points to an array
    - Properties of arrays:
        - Fixed size
        - Array type is size and underlying type
        - No initialization (0 values)
        - Not a pointer!
- Methods and Interfaces
    - Any type that has the same methods as an interface implements that interface
    - [http://golang.org/doc/effective_go.html#methods](http://golang.org/doc/effective_go.html#methods)
    - [http://golang.org/doc/effective_go.html#interfaces](http://golang.org/doc/effective_go.html#interfaces)
- Concurrency  
    * **Do not communicate by sharing memory, instead share memory by communicating**
        - https://blog.golang.org/share-memory-by-communicating
        - https://golang.org/doc/codewalk/sharemem/
        - https://coderwall.com/p/rklk_a/go-share-memory-by-communicating
    * **Goroutines**
        - Lightweight thread
        - Managed by Go runtime
        - Just use the keyword "go"
    * **Channels** - like UNIX pipes
    * **Select** statement:
        - Like a switch, but on communications
        - Rules:
            - Execute case that is "ready"
            - If more than one is "ready" execute one at random
            - If none are "ready" block, unless default is defined
- Conclusion: [http://golang.org/doc/effective_go.html](http://golang.org/doc/effective_go.html)
